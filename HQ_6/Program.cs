﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задача 6 часть 2я
            int x, y, i = 0;
            int sum = 0;

            Console.Write("Введите х: ");
            x = int.Parse(Console.ReadLine());

            Console.Write("Введите y: ");
            y = int.Parse(Console.ReadLine());

            while(y>i && x<y)
            {
                sum = x + sum;
                i++;
            }

            while (x > i && y < x)
            {
                sum = y + sum;
                i++;
            }

            while(x==y && x>i)
            {
                sum = y + sum;
                i++;
            }

            Console.Write(sum);
            Console.ReadKey();
        }
    }
}
