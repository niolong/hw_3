﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_10
{
    class Program
    {
        static void Main(string[] args)
        {           
            int i=0;
            int num=100;
            
            while (i < 10 )                
            {
                if (num%10==7 && num%9==0)
                {
                    Console.WriteLine($"Число соответствует условию {num}");
                    i++;
                }
                num++;
            }

            Console.ReadKey();
        }
    }
}
