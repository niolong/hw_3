﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            int sum;

            Console.Write("от: ");
            a = int.Parse(Console.ReadLine());

            Console.Write("до: ");
            b = int.Parse(Console.ReadLine());

            sum = 0;

            while (a<=b)
            {
                sum = sum * a;
                a++;
            }
            Console.WriteLine(sum);

            Console.ReadKey();
        }
    }
}
