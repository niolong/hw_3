﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            int i=0;
            int a=0, b=0;

            Console.Write("Введите последовательность из 10 чисел ");

            while (i<10)
            {
                Console.Write($"{i+1} число: ");
                num = int.Parse(Console.ReadLine());

                if(num==7)
                {
                    a += 1;
                }
                else if (num==5)
                {
                    b += 1;
                }
                i++;               
            }

            if(a>b)
            {
                Console.WriteLine("В последовательности больше 7 ");
            }
            else if (a<b)
            {
                Console.WriteLine("В последовательности больше 5 ");
            }
            else
            {
                Console.WriteLine("В последовательности нету 7 и 5");
            }

            Console.ReadKey();
        }
    }
}
