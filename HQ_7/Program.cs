﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int num, findNum=1;
        
            Console.Write("Введите число: ");
            num = int.Parse(Console.ReadLine());
            
            while (num>findNum)
            {
                findNum = findNum * 3;               
            }

            if(findNum==num)
            {
                Console.WriteLine("Является числом степени 3");
            }
            else
            {
                Console.WriteLine("Не является числом степени 3");
            }

            Console.ReadKey();

        }
    }
}
