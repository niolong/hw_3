﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b,c;
            double sum;

            Console.Write("от: ");
            a = int.Parse(Console.ReadLine());

            Console.Write("до: ");
            b = int.Parse(Console.ReadLine());

            c = a;
            sum = 0;

            while(a<=b)
            {
                sum = a + sum;
                a++;
            }

            sum = sum/(b - c+1);

            Console.WriteLine($"Среднее арифметическое = {sum}");

            Console.ReadKey();
        }
    }
}
