﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1, n;
            double sum, way;

            Console.Write("введите количество этапов: ");
            n = int.Parse(Console.ReadLine());

            way = 0;
            sum = 0;

            while (n >= i)
            {
                if (i % 2 == 0)
                {
                    sum = 1000 / i+way ;
                }
                else
                {
                    sum = 1000 / i-way;
                }
                way = sum;
                             
                i++;
            }

            Console.WriteLine($"1/{i-1}={sum}м");

            Console.ReadKey();
        }
    }
}
