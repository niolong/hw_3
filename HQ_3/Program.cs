﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_3
{
    class Program
    {
        static void Main(string[] args)
        {           
            double b=1, n;
            double sum;

            Console.Write("введите n : ");
            n = int.Parse(Console.ReadLine());

            sum = 0.0;

           while (n>=b)
            {
                sum = 1 / b + sum;
                b++;             
            }

            Console.WriteLine($"сумма = {sum}");

            Console.ReadKey();
        }
    }
}
