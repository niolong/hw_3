﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HQ_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int i=1,n,sum=0;

            Console.Write("Введите число для возведения в квадрат: ");
            n = int.Parse(Console.ReadLine());

            while(i<=2*n-1)
            {
                sum += i;
                i += 2;
            }

            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
